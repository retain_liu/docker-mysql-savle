## 启动docker-compose
docker-compose up -d
## master 设置
```
#进入master
docker exec -it mysql-master bash  

#进入mysql
mysql -u root -p

#创建一个用户来同步数据，这里表示创建一个slaver同步账号backup，允许访问的IP地址为%，%表示通配符
GRANT REPLICATION SLAVE ON *.* to 'backup'@'%' identified by '123456';

#刷新权限
flush privileges;

//查看状态，记住File、Position的值，在Slaver中将用到
show master status;
```
![Alt text](image/master_status.png)  


## slave  设置
```
#进入slaver
docker exec -it mysql-slave bash

#进入mysql
mysql -u root -p

#设置主库链接
change master to master_host='192.168.14.231',master_user='backup',master_password='123456',master_log_file='mysql-bin.000003',master_log_pos=1131,master_port=3306;

#启动从库同步
start slave;

#查看状态
show slave status\G;
```
同时YES表示启动成功  
![Alt text](image/slave_status.png) 

## 注意
binlog-ignore-db表示忽略同步数据  
binlog-do-db表示需要同步的数据库
 
